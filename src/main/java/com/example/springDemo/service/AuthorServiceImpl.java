package com.example.springDemo.service;

import com.example.springDemo.entity.Author;
import com.example.springDemo.repo.AuthorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;
@Service
public class AuthorServiceImpl implements AuthorService{
    @Autowired
    private AuthorRepo authorRepo;
    @Override
    public List<Author> getAllAuthors() {
        return (List<Author>) authorRepo.findAll();
    }


    @Override
    public void saveAuthor(@Valid Author author) {

        authorRepo.save(author);
    }

    @Override
    public Author updateAuthor(int id) {

        return authorRepo.findById(id).get();
    }

    @Override
    public void deleteAuthor(int id) {
        authorRepo.deleteById(id);
    }
}
