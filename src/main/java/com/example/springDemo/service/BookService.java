package com.example.springDemo.service;

import com.example.springDemo.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import java.util.List;

public interface BookService {
    List<Book> getAllBooks();
    void saveBook(@Valid Book book);
    Book updateBook(int id);
    void deleteBook(int id);
    //Page<Book> findByTitleContainingIgnoreCase(String keyword, Pageable pageable);
    Page<Book> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

}
