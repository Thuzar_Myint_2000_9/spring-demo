package com.example.springDemo.service;

import com.example.springDemo.entity.Book;
import com.example.springDemo.repo.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService{
    @Autowired
    private BookRepo bookRepo;
    @Override
    public List<Book> getAllBooks() {
        return (List<Book>) bookRepo.findAll();
    }

    @Override
    public void saveBook(@Valid Book book) {
        bookRepo.save(book);
    }

    @Transactional
    @Override
    public Book updateBook(int id) {

        Optional<Book> optional = bookRepo.findById(id);
        Book book = null;
        if(optional.isPresent()){
            book = optional.get();
        }else {
            throw new RuntimeException("Book not found " + id);
        }
        return book;
    }

    @Override
    public void deleteBook(int id) {
        this.bookRepo.deleteById(id);
    }

    @Override
    public Page<Book> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);
        return this.bookRepo.findAll(pageable);
    }
}
