package com.example.springDemo.service;

import com.example.springDemo.entity.Author;

import javax.validation.Valid;
import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthors();
    void saveAuthor(@Valid Author author);
    Author updateAuthor(int id);
    void deleteAuthor(int id);
}
