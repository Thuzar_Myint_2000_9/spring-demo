package com.example.springDemo.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotBlank(message = "Enter your name")
    private String name;
    @NotBlank(message = "Enter your address")
    private String address;
    @NotBlank(message = "Enter your email")
    @Email(message = "Enter your valid email ")
    private String email;
    @OneToMany(mappedBy = "author",cascade = CascadeType.ALL)
   private List<Book> books = new ArrayList<>();

    public Author(String name, String address, String email, List<Book> books) {
        this.name = name;
        this.address = address;
        this.email = email;
        this.books = books;
    }
}
