package com.example.springDemo.controller;

import com.example.springDemo.entity.Author;
import com.example.springDemo.entity.Book;
import com.example.springDemo.repo.AuthorRepo;
import com.example.springDemo.repo.BookRepo;
import com.example.springDemo.service.AuthorServiceImpl;
import com.example.springDemo.service.BookServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Controller
@Slf4j
public class HelloController {
    @Autowired
    private BookServiceImpl bookService;
    @Autowired
    private BookRepo bookRepo;
    @Autowired
    private AuthorServiceImpl authorService;
    @Autowired
    private AuthorRepo authorRepo;

    @RequestMapping("/")
    public String hello() {
      return "index";
   }

    @GetMapping("/list/book")
    public String getAllBooks(Model model){
        List<Author> authorList = authorService.getAllAuthors();
        model.addAttribute("books",bookService.getAllBooks());
        model.addAttribute("authorList",authorList);
        return "list_book";
        //return findPaginated(1, 5,"title", "asc", model);
    }

    @GetMapping("/create/book")
    public String createBook(Model model) {
        List<Author> authorList = authorService.getAllAuthors();
        Book book = new Book();
        model.addAttribute("book",book);
        model.addAttribute("authorList",authorList);
        return "create_book";
    }

    @PostMapping("/save/book")
    public String saveEmployee(@Valid Book book,  BindingResult result, Model model) {

        if (result.hasErrors()) {
            List<Author> authorList = authorService.getAllAuthors();
            model.addAttribute("authorList",authorList);
            return "create_book";
        }
        bookService.saveBook(book);
        return "redirect:/list/book";
    }



    @GetMapping("/update/book/{id}")
    public String updateBooks(@PathVariable int id, Model model) {
        List<Author> authorList = authorService.getAllAuthors();
        Book book = bookService.updateBook(id);

        model.addAttribute("book",book);
        model.addAttribute("authorList",authorList);
        //bookService.saveBook(book);
        return "update_book";
    }

    @PostMapping("/update/{id}")
    public String updateBookId(@PathVariable("id") int id, @Valid Book book, BindingResult result, Model model) {
        //bookRepo.save(book);
            if (result.hasErrors()) {
                List<Author> authorList = authorService.getAllAuthors();
                model.addAttribute("authorList",authorList);
                book.setId(id);
                return "update_book";
            }

        bookService.saveBook(book);
        return "redirect:/list/book";
    }

    @GetMapping("/delete/book/{id}")
    public String deleteBooks(@PathVariable int id) {
        bookService.deleteBook(id);
        return "redirect:/list/book";
    }

    @GetMapping("/list/author")
    public String getAllAuthors(Model model){
        model.addAttribute("authors",authorService.getAllAuthors());
        return "list_author";
    }

    @PostMapping("/save/author")
    public String addBook(@Valid Author author, BindingResult result) {

        if (result.hasErrors()) {
            return "create_author";
        }
        authorService.saveAuthor(author);
        return "redirect:/list/author";
    }

    @GetMapping("/create/author")
    public String createAuthor(Model model) {
       Author author = new Author();
        model.addAttribute("author",author);
        return "create_author";
    }

    @GetMapping("/update/author/{id}")
    public String editBook(@PathVariable("id") int id,Model model) {
        Author author=authorRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid author Id:" + id));;
        model.addAttribute("author",author);
        return "update_author";
    }

    @PostMapping("/edit/{id}")
    public String updateUser(@PathVariable("id") int id, @Valid Author author, BindingResult result) {

        if (result.hasErrors()) {
            author.setId(id);
            return "update_author";
        }

        authorService.saveAuthor(author);
        return "redirect:/list/author";

    }

    @GetMapping("/delete/author/{id}")
    public String deleteBook(@PathVariable("id")int id) {
        authorService.deleteAuthor(id);
        return "redirect:/list/author";
    }

//    @GetMapping("/page/{pageNo}/{pageSize}")
//    public String findPaginated(@PathVariable (value = "pageNo") int pageNo,
//                                @PathVariable(value = "pageSize") int pageSize,
//                                @RequestParam("sortField") String sortField,
//                                @RequestParam("sortDir") String sortDir,
//                                Model model) {
//        //int pageSize = 5;
//
//        Page<Book> page = bookService.findPaginated(pageNo, pageSize, sortField, sortDir);
//        List<Book> listBook = page.getContent();
//
//        model.addAttribute("currentPage", pageNo);
//        model.addAttribute("pageSize", pageSize);
//        model.addAttribute("totalPages", page.getTotalPages());
//        model.addAttribute("totalItems", page.getTotalElements());
//
//        model.addAttribute("sortField", sortField);
//        model.addAttribute("sortDir", sortDir);
//        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
//
//        List<Author> authorList = authorService.getAllAuthors();
//        model.addAttribute("books",listBook);
//        model.addAttribute("authorList",authorList);
//        return "list_book";
//    }


}
