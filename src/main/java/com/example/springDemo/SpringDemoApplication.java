package com.example.springDemo;

import com.example.springDemo.entity.Author;
import com.example.springDemo.entity.Book;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDate;

@SpringBootApplication
public class SpringDemoApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringDemoApplication.class, args);
	}

	@Bean
	@Transactional
	public ApplicationRunner runner() {
		return r -> {

		};
	}

}
